## 1.0.6

* **Platform Interface**
  * **evaluateJavascript** now returns a proper **null** value instead of **"null"**.
  * Added **getUrl** function.
  * Added **getOriginalUrl** function.

* **Settings**
  * Changed type of **onPageStarted** from a callback function to a broadcast stream.
  * Changed type of **onPageFinished** from a callback function to a broadcast stream.
  * Changed type of **onProgress** from a callback function to a broadcast stream.
  * Changed type of **onWebResourceError** from a callback function to a broadcast stream.


## 1.0.5

* Changed library structure, now using **part** and **part of**.

## 1.0.4

* Fixed a bug in the **initialize** method, where **_invoke** was used
  to update the native settings when the WebView was not yet initialized.
* Removed **ResultError** exception.
* Updated example.

## 1.0.3

* Changed the way **Settings** updates the native settings.

## 1.0.2

* Added export of **ResultError**

## 1.0.1

* Added **ResultError** for invalid results
* Commented the webkit API functions
* Added **clearCookies** function
* Added **clearCache** function
* Added **reload** function
* Added README.md
  
## 1.0.0

* First working version
