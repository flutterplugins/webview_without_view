package com.twelve_ampere.webview_without_view;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.Map;

import io.flutter.plugin.common.MethodChannel;

public class InvisibleWebView {
    private WebView webView;
    private InvisibleWebViewClient webViewClient;
    private JavaScriptChannel jsChannel;
    private Handler platformThreadHandler;
    private final MethodChannel methodChannel;
    private boolean isJSChannelAdded;

    @SuppressWarnings("unchecked")
    InvisibleWebView(final Context context,
                     Map<String, Object> params,
                     MethodChannel channel) {
        DisplayListenerProxy displayListenerProxy = new DisplayListenerProxy();
        DisplayManager displayManager =
                (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
        displayListenerProxy.onPreWebViewInitialization(displayManager);

        Boolean usesHybridComposition = (Boolean) params.get("usesHybridComposition");
        webView =
                (usesHybridComposition)
                        ? new WebView(context)
                        : new InputAwareWebView(context, null);

        displayListenerProxy.onPostWebViewInitialization(displayManager);

        platformThreadHandler = new Handler(context.getMainLooper());

        // Allow local storage.
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        // Multi windows is set with FlutterWebChromeClient by default to handle internal bug: b/159892679.
        webView.getSettings().setSupportMultipleWindows(true);
        webView.setWebChromeClient(new FlutterWebChromeClient());
        webView.setVisibility(View.GONE);

        methodChannel = channel;
        webViewClient = new InvisibleWebViewClient(methodChannel);
        webView.setWebViewClient(webViewClient.createWebViewClient(false));
        isJSChannelAdded = false;

        Map<String, Object> settings = (Map<String, Object>) params.get("settings");
        if (null != settings) applySettings(settings);

        if (params.containsKey("initialUrl")) {
            String url = (String) params.get("initialUrl");
            webView.loadUrl(url);
        }
    }

    private void updateAutoMediaPlaybackPolicy(int mode) {
        // This is the index of the AutoMediaPlaybackPolicy enum, index
        // 1 is always_allow, for all other values we require a user gesture.
        boolean requireUserGesture = mode != 1;
        if (Utils.isSDKOrAbove(Build.VERSION_CODES.JELLY_BEAN_MR1))
            webView.getSettings().setMediaPlaybackRequiresUserGesture(requireUserGesture);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void updateJsMode(int mode) {
        switch (mode) {
            case 0: // disabled
                webView.getSettings().setJavaScriptEnabled(false);
                break;

            case 1: // unrestricted
                webView.getSettings().setJavaScriptEnabled(true);
                break;

            default:
                throw new IllegalArgumentException("Trying to set unknown JavaScript mode: " + mode);
        }
    }

    private void updateUserAgent(String userAgent) {
        webView.getSettings().setUserAgentString(userAgent);
    }

    public void applySettings(Map<String, Object> settings) {
        for (String key : settings.keySet()) {

            if (key.equals("jsMode")) {
                Integer mode = (Integer) settings.get(key);
                if (null == mode) continue;
                updateJsMode(mode);
                continue;
            }

            if (key.equals("debuggingEnabled")) {
                boolean debuggingEnabled = (boolean) settings.get(key);

                if (Utils.isSDKOrAbove(Build.VERSION_CODES.KITKAT))
                    WebView.setWebContentsDebuggingEnabled(debuggingEnabled);

                continue;
            }

            if (key.equals("hasProgressTracking")) {
                webViewClient.hasProgressTracking = (boolean) settings.get(key);
                continue;
            }

            if (key.equals("userAgent")) {
                updateUserAgent((String) settings.get(key));
                continue;
            }

            if (key.equals("allowsInlineMediaPlayback")) {
                // no-op inline media playback is always allowed on Android.
                continue;
            }

            if (key.equals("jsChannelName")) {
                String name = (String) settings.get(key);
                if (isJSChannelAdded) webView.removeJavascriptInterface(name);

                jsChannel = new JavaScriptChannel(
                        methodChannel,
                        name,
                        platformThreadHandler
                );

                webView.addJavascriptInterface(jsChannel, name);
                isJSChannelAdded = true;

                continue;
            }

            if (key.equals("autoMediaPlaybackPolicy")) {
                Integer autoMediaPlaybackPolicy = (Integer) settings.get(key);
                if (null == autoMediaPlaybackPolicy) continue;
                updateAutoMediaPlaybackPolicy(autoMediaPlaybackPolicy);
                continue;
            }

            if (key.equals("hasNavigationDelegate")) {
                webViewClient.hasNavigationDelegate = (boolean) settings.get(key);
                return;
            }

            throw new IllegalArgumentException("Unknown WebView setting: " + key);
        }
    }

    public void dispose() {
        if (webView instanceof InputAwareWebView)
            ((InputAwareWebView) webView).dispose();
        webView.destroy();

        webView = null;
        webViewClient = null;
        jsChannel = null;
        platformThreadHandler = null;
    }

    public void loadUrl(String url, Map<String, String> headers) {
        if (null == headers) headers = Collections.emptyMap();
        webView.loadUrl(url, headers);
    }

    /**
     * script cannot be null.
     */
    public void evaluateJavascript(String script, final MethodChannel.Result result) {
        webView.evaluateJavascript(script, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                result.success(value);
            }
        });
    }

    public void clearCache() {
        webView.clearCache(true);
        WebStorage.getInstance().deleteAllData();
    }

    public void reload() {
        webView.reload();
    }

    public String getUrl() {
        return webView.getUrl();
    }

    public String getOriginalUrl() {
        return webView.getOriginalUrl();
    }

    // Verifies that a url opened by `Window.open` has a secure url.
    private class FlutterWebChromeClient extends WebChromeClient {
        @Override
        public boolean onCreateWindow(
                final WebView view,
                boolean isDialog,
                boolean isUserGesture,
                Message resultMsg
        ) {
            final WebViewClient webViewClient =
                    new WebViewClient() {

                        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                        @Override
                        public boolean shouldOverrideUrlLoading(
                                @NonNull WebView view, @NonNull WebResourceRequest request) {
                            final String url = request.getUrl().toString();
                            if (!InvisibleWebView.this.webViewClient.shouldOverrideUrlLoading(
                                    InvisibleWebView.this.webView, request))
                                webView.loadUrl(url);

                            return true;
                        }

                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            if (!InvisibleWebView.this.webViewClient.shouldOverrideUrlLoading(
                                    InvisibleWebView.this.webView, url))
                                webView.loadUrl(url);

                            return true;
                        }
                    };

            final WebView newWebView = new WebView(view.getContext());
            newWebView.setWebViewClient(webViewClient);

            final WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(newWebView);
            resultMsg.sendToTarget();

            return true;
        }

        @Override
        public void onProgressChanged(WebView view, int progress) {
            webViewClient.onLoadingProgress(progress);
//            Map<String, Object> args = new HashMap<>();
//            args.put("progress", progress);
//            methodChannel.invokeMethod("onProgress", args);
        }
    }
}
