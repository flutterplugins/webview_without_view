package com.twelve_ampere.webview_without_view;

import android.os.Build;

public final class Utils {

    public static boolean isSDKOrAbove(int versionCode) {
        return Build.VERSION.SDK_INT >= versionCode;
    }

    private Utils() {
    }
}
