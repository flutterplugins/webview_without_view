package com.twelve_ampere.webview_without_view;

import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;

import androidx.annotation.NonNull;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

final class FlutterCookieManager {

    public static void clearCookies(final MethodChannel.Result result) {
        CookieManager cookieManager = CookieManager.getInstance();
        final boolean hasCookies = cookieManager.hasCookies();

        if (Utils.isSDKOrAbove(Build.VERSION_CODES.LOLLIPOP)) {
            cookieManager.removeAllCookies(
                    new ValueCallback<Boolean>() {
                        @Override
                        public void onReceiveValue(Boolean value) {
                            result.success(hasCookies);
                        }
                    });
            return;
        }

        cookieManager.removeAllCookie();
        result.success(hasCookies);
    }
}