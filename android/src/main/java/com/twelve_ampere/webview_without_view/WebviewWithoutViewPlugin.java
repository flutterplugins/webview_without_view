package com.twelve_ampere.webview_without_view;

import androidx.annotation.NonNull;

import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

public class WebviewWithoutViewPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
    private static final String NAMESPACE = "com.twelve_ampere.webview_without_view.WebviewWithoutViewPlugin";
    private static final String METHODCHANNEL_ID = NAMESPACE + ".methods";

    private static final String LOG_TAG = WebviewWithoutViewPlugin.class.getSimpleName();

    private InvisibleWebView webView;
    private MethodChannel channel;
    private ActivityPluginBinding activityBinding;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        BinaryMessenger messenger = flutterPluginBinding.getBinaryMessenger();
        channel = new MethodChannel(messenger, METHODCHANNEL_ID);
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        dispose();
        channel.setMethodCallHandler(null);
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        activityBinding = binding;
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        activityBinding = null;
    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
        activityBinding = binding;
    }

    @Override
    public void onDetachedFromActivity() {
        activityBinding = null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (call.method.equals("initialize")) {

            if (null == activityBinding) {
                result.success(false);
                return;
            }

            Map<String, Object> params = (Map<String, Object>) call.arguments;
            webView = new InvisibleWebView(
                    activityBinding.getActivity().getApplicationContext(),
                    params,
                    channel
            );

            result.success(true);
            return;
        }

        if (call.method.equals("dispose")) {
            dispose();
            result.success(null);
            return;
        }

        if (call.method.equals("_updateSettings")) {
            Map<String, Object> settings = (Map<String, Object>) call.arguments;
            webView.applySettings(settings);

            result.success(null);
            return;
        }

        if (call.method.equals("loadUrl")) {
            String url = call.argument("url");
            Map<String, String> headers = call.argument("additionalHeaders");
            webView.loadUrl(url, headers);

            result.success(null);
            return;
        }

        if (call.method.equals("evaluateJavascript")) {
            String script = call.argument("script");
            if (null == script) {
                result.error(
                        ErrorCodes.NULL_VALUE,
                        "JavaScript string cannot be null",
                        null
                );
                return;
            }

            webView.evaluateJavascript(script, result);
            return;
        }

        if (call.method.equals("clearCookies")) {
            FlutterCookieManager.clearCookies(result);
            result.success(null);
            return;
        }

        if (call.method.equals("clearCache")) {
            webView.clearCache();
            result.success(null);
            return;
        }

        if (call.method.equals("reload")) {
            webView.reload();
            result.success(null);
            return;
        }

        if (call.method.equals("getUrl")) {
            result.success(webView.getUrl());
            return;
        }

        if (call.method.equals("getOriginalUrl")) {
            result.success(webView.getOriginalUrl());
            return;
        }

        result.notImplemented();
    }

    private void dispose() {
        if (null != webView) webView.dispose();
        webView = null;
        activityBinding = null;
    }
}
