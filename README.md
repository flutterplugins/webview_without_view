## Introduction

A flutter plugin which allows the usage of the android webkit API without needing to display anything.

## Setup

### Android

* The following permission has to be added to your **AndroidManifest.xml**.
```xml
<uses-permission android:name="android.permission.INTERNET" />
```

### IOS

* Not yet implemented, because I currently do not have access to a machine running MacOS. 
  
## Usage

1. The plugin has to be initialized before being able to use it. It
   is advised to do this in the main function, before the **runApp**
   function is called.
```dart
import 'package:flutter/material.dart';
import 'package:webview_without_view/webview_without_view.dart' as webview;

import 'home_page.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await webview.initialize();

  runApp(HomePage());
} 

```

2. After successfully initializing the webview_without_view plugin, you can
   load a page and retrieve the HTML source, if you wish to. (See the example app for more information)

3. When you are done with using the functions from webview_without_view, call
   **dispose**.
```dart
  @override
  void dispose() {
    webview.dispose();
    super.dispose();
  }
``` 

