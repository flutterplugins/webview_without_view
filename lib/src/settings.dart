part of webview_without_view;

class _Settings {
  /// Whether any members of this [Settings] instance have
  /// changed or not.
  bool _hasChanged;

  JavascriptMode _javascriptMode;
  JavascriptChannel? _javascriptChannel;
  bool _allowsInlineMediaPlayback;
  bool _debuggingEnabled;
  String? _userAgent;
  AutoMediaPlaybackPolicy _autoMediaPlaybackPolicy;

  final StreamController<String> _onPageStartedController;
  final StreamController<String> _onPageFinishedController;
  final StreamController<int> _onProgressController;
  final StreamController<WebResourceError> _onWebResourceErrorController;

  /// A delegate function that decides how to handle
  /// navigation actions. When a navigation is initiated by the WebView
  /// (e.g when a user clicks a link) this delegate is called and has to decide
  /// how to proceed with the navigation. See [NavigationDecision] for possible
  /// decisions the delegate can take. When null all navigation actions are allowed.
  ///
  /// * *Caveats on Android*:
  ///   * Navigation actions targeted to the main frame can be intercepted,
  ///     navigation actions targeted to subframes are allowed regardless of
  ///     the value returned by this delegate.
  ///
  ///   * Setting a navigationDelegate makes the WebView treat all navigations
  ///     as if they were triggered by a user gesture, this disables some of
  ///     Chromium's security mechanisms. A navigationDelegate should only be
  ///     set when loading trusted content.
  ///
  ///   * On Android WebView versions earlier than 67(most devices running at least
  ///     Android L+ should have a later version):
  ///
  ///     * When a navigationDelegate is set pages with frames are not properly
  ///       handled by the webview, and frames will be opened in the main frame.
  ///
  ///     * When a navigationDelegate is set HTTP requests do not include the
  ///       HTTP referer header.
  NavigationDelegate? navigationDelegate;

  /// Emits an event with the current url when
  /// a page starts loading.
  ///
  /// This is a `BroadcastStream`.
  Stream<String> get onPageStarted =>
      _onPageStartedController.stream.asBroadcastStream();

  /// Emits an event with the current url when a page has
  /// finished loading.
  /// This is invoked only for the main frame.
  /// When [onPageFinished] is invoked on Android, the page being rendered may
  /// not be updated yet.
  /// When invoked on iOS or Android, any Javascript code that is embedded
  /// directly in the HTML has been loaded and code injected with
  /// [evaluateJavascript] can assume this.
  ///
  /// This is a `BroadcastStream`.
  Stream<String> get onPageFinished =>
      _onPageFinishedController.stream.asBroadcastStream();

  /// Emits an event every time a page made some
  /// progress while loading.
  ///
  /// This is a `BroadcastStream`.
  Stream<int> get onProgress =>
      _onProgressController.stream.asBroadcastStream();

  /// Emits an event when a web resource has failed to load.
  /// This can be called for any resource (iframe, image, etc.), not just for
  /// the main page.
  ///
  /// This is a `BroadcastStream`.
  Stream<WebResourceError> get onWebResourceError =>
      _onWebResourceErrorController.stream.asBroadcastStream();

  _Settings._()
      : _javascriptMode = JavascriptMode.disabled,
        _allowsInlineMediaPlayback = true,
        _debuggingEnabled = false,
        _autoMediaPlaybackPolicy =
            AutoMediaPlaybackPolicy.requireUserActionForAllMediaTypes,
        _hasChanged = false,
        // StreamControllers
        _onPageStartedController = StreamController.broadcast(),
        _onPageFinishedController = StreamController.broadcast(),
        _onProgressController = StreamController.broadcast(),
        _onWebResourceErrorController = StreamController.broadcast();

  Map<String, Object?> _toMap() => {
        "jsMode": javascriptMode.index,
        "debuggingEnabled": debuggingEnabled,
        "hasProgressTracking": hasProgressTracking,
        "hasNavigationDelegate": hasNavigationDelegate,
        "allowsInlineMediaPlayback": allowsInlineMediaPlayback,
        "autoMediaPlaybackPolicy": settings.autoMediaPlaybackPolicy.index,
        "userAgent": userAgent,
        if (null != javascriptChannel) "jsChannelName": javascriptChannel!.name,
      };

  bool get hasProgressTracking => _onProgressController.hasListener;

  bool get hasNavigationDelegate => null != settings.navigationDelegate;

  /// The value used for the HTTP User-Agent: request header.
  /// When null the platform's webview default is used for the User-Agent header.
  /// When the [WebView] is rebuilt with a different `userAgent`, the page
  /// reloads and the request uses the new User Agent.
  /// When [WebViewController.goBack] is called after changing `userAgent`
  /// the previous `userAgent` value is used until the page is reloaded.
  /// This field is ignored on iOS versions prior to 9 as the platform does
  /// not support a custom user agent.
  String? get userAgent => _userAgent;

  /// Whether Javascript execution is enabled.
  /// Defaults to [JavascriptMode.disabled]
  JavascriptMode get javascriptMode => _javascriptMode;

  /// The set of [JavascriptChannel]s available to
  /// JavaScript code running in the web view.
  /// For each [JavascriptChannel] in the set, a channel object is made
  /// available for the JavaScript code in a window property named
  /// [JavascriptChannel.name].
  /// The JavaScript code can then call `postMessage` on that object to
  /// send a message that will be passed to [JavascriptChannel.onMessageReceived].
  /// For example for the following JavascriptChannel:
  /// ```dart
  /// JavascriptChannel(name: 'Print', onMessageReceived: (JavascriptMessage message) { print(message.message); });
  /// ```
  /// JavaScript code can call:
  /// ```javascript
  /// Print.postMessage('Hello');
  /// ```
  /// To asynchronously invoke the message handler which will print the
  /// message to standard output.
  /// Adding a new JavaScript channel only takes affect after the next page is loaded.
  /// Set values must not be null. A [JavascriptChannel.name] cannot be the same
  /// for multiple channels in the list.
  /// A null value is equivalent to an empty set.
  JavascriptChannel? get javascriptChannel => _javascriptChannel;

  /// Controls whether inline playback of HTML5 videos is allowed
  /// on iOS. This field is ignored on Android because Android
  /// allows it by default.
  ///
  /// Defaults to `true`.
  bool get allowsInlineMediaPlayback => _allowsInlineMediaPlayback;

  /// Controls whether WebView debugging is enabled.
  /// Setting this to true enables [WebView debugging on Android](https://developers.google.com/web/tools/chrome-devtools/remote-debugging/).
  /// WebView debugging is enabled by default in dev builds on iOS.
  /// Defaults to `false`.
  ///
  /// * *To debug WebViews on iOS*:
  ///   * Enable developer options (Open Safari, go to Preferences ->
  ///     Advanced and make sure "Show Develop Menu in Menubar" is on.)
  ///
  ///   * From the Menu-bar (of Safari) select Develop -> iPhone Simulator
  ///     -> <your webview page>
  bool get debuggingEnabled => _debuggingEnabled;

  /// Which restrictions apply on automatic media playback.
  /// This initial value is applied to the platform's webview upon creation.
  /// Any following changes to this parameter are ignored (as long as the
  /// state of the [WebView] is preserved).
  ///
  /// Defaults to [AutoMediaPlaybackPolicy.requireUserActionForAllMediaTypes].
  AutoMediaPlaybackPolicy get autoMediaPlaybackPolicy =>
      _autoMediaPlaybackPolicy;

  set userAgent(String? agent) {
    _userAgent = agent;
    _hasChanged = true;
  }

  set javascriptMode(JavascriptMode mode) {
    _javascriptMode = mode;
    _hasChanged = true;
  }

  set javascriptChannel(JavascriptChannel? channel) {
    _javascriptChannel = channel;
    _hasChanged = true;
  }

  set allowsInlineMediaPlayback(bool allowed) {
    _allowsInlineMediaPlayback = allowed;
    _hasChanged = true;
  }

  set debuggingEnabled(bool enabled) {
    _debuggingEnabled = enabled;
    _hasChanged = true;
  }

  set autoMediaPlaybackPolicy(AutoMediaPlaybackPolicy policy) {
    _autoMediaPlaybackPolicy = policy;
    _hasChanged = true;
  }

  Future<void> _dispose() => Future.wait([
        _onPageStartedController.close(),
        _onPageFinishedController.close(),
        _onProgressController.close(),
        _onWebResourceErrorController.close(),
      ]);
}
