part of webview_without_view;

const String _namespace =
    "com.twelve_ampere.webview_without_view.WebviewWithoutViewPlugin";
const String _methodChannelId = "$_namespace.methods";

MethodChannel? _channel;
_Settings? _settings;

_Settings get settings => _settings ??= _Settings._();

/// Before using any functions, this plugin has to be
/// initialized.
///
/// Nothing happens if [initialize] is called more than
/// once.
///
/// If not `null`, then [initialUrl] is the initial
/// URL to load.
Future<bool> initialize([String? initialUrl]) async {
  if (null != _channel) return true;

  _channel = const MethodChannel(_methodChannelId);

  final params = {
    "usesHybridComposition": true,
    "settings": settings._toMap(),
    if (null != initialUrl) "initialUrl": initialUrl
  };

  settings._hasChanged = false;

  if (!(await _channel!.invokeMethod<bool>(
        "initialize",
        params,
      ) ??
      false)) return false;

  _channel!.setMethodCallHandler(_onMethodCallHandler);

  return true;
}

/// Calling [dispose] cuts the connection to the
/// platform code and therefore removes the functionality
/// of any functions in this library.
Future<void> dispose() async {
  // Dont use [_invoke] here, because we do
  // not care about changing the settings anymore.
  await _channel?.invokeMethod("dispose");

  _channel?.setMethodCallHandler(null);
  _channel = null;

  await _settings?._dispose();
}

/// Loads the given URL with additional HTTP headers, specified
/// as a map from name to value.
Future<void> loadUrl(
  String url, [
  Map<String, String>? additionalHeaders,
]) =>
    _invoke("loadUrl", {
      "url": url,
      "additionalHeaders": additionalHeaders,
    });

/// Asynchronously evaluates JavaScript in the context of
/// the currently loaded page.
///
/// Always returns `null`, if [settings.javascriptMode] equals
/// [JavascriptMode.disabled].
Future<String?> evaluateJavascript(String script) async {
  final result = await _invoke<String?>("evaluateJavascript", {
    "script": script,
  });

  if ("null" == result) return null;

  return result;
}

/// Removes all cookies.
Future<void> clearCookies() => _invoke("clearCookies");

/// Clears the resource cache and all storage currently being
/// used by the JavaScript storage APIs. This includes the
/// Application Cache, Web SQL Database and the HTML5 Web Storage APIs.
Future<void> clearCache() => _invoke("clearCache");

/// Reloads the current URL.
Future<void> reload() => _invoke("reload");

/// Gets the URL for the current page. This is not always
/// the same as the URL passed to `onPageStarted` because
/// although the load for that URL has begun, the current
/// page may not have changed.
///
/// Return the URL for the current page or `null` if no page
/// has been loaded.
Future<String?> getUrl() => _invoke<String?>("getUrl");

/// Gets the original URL for the current page. This is not
/// always the same as the URL passed to `onPageStarted`
/// because although the load for that URL has begun, the
/// current page may not have changed. Also, there may have
/// been redirects resulting in a different URL to that
/// originally requested.
///
/// Returns the URL that was originally requested for
/// the current page or `null` if no page has been loaded.
Future<String?> getOriginalUrl() => _invoke<String?>("getOriginalUrl");

Future<T?> _invoke<T>(String method, [Map<String, Object?>? arguments]) async {
  if (null == _channel) return null;

  if (settings._hasChanged) {
    final args = settings._toMap();
    settings._hasChanged = false;
    await _channel!.invokeMethod("_updateSettings", args);
  }

  return _channel!.invokeMethod(method, arguments);
}

Future<dynamic> _onMethodCallHandler(MethodCall call) async {
  if ("javascriptChannelMessage" == call.method) {
    // Not needed because there is only one channel
    // final channelName = call.arguments["channel"] as String;
    final message = call.arguments["message"] as String;
    settings.javascriptChannel?.onMessageReceived(JavascriptMessage(message));
    return null;
  }

  if ("onProgress" == call.method) {
    final progress = call.arguments["progress"] as int;
    settings._onProgressController.add(progress);
    return null;
  }

  if ("onPageStarted" == call.method) {
    final url = call.arguments["url"] as String;
    settings._onPageStartedController.add(url);
    return null;
  }

  if ("onPageFinished" == call.method) {
    final url = call.arguments["url"] as String;
    settings._onPageFinishedController.add(url);
    return null;
  }

  if ("onWebResourceError" == call.method) {
    final errorCode = call.arguments["errorCode"] as int;
    final description = call.arguments["description"] as String;
    final failingUrl = call.arguments["failingUrl"] as String;
    final errorTypeString = call.arguments["errorType"] as String?;
    final domain = call.arguments["domain"] as String?;

    settings._onWebResourceErrorController.add(WebResourceError._(
      errorCode: errorCode,
      description: description,
      failingUrl: failingUrl,
      domain: domain,
      errorType: null == errorTypeString
          ? null
          : WebResourceErrorType.values.firstWhere(
              (element) =>
                  "$WebResourceErrorType.$errorTypeString" ==
                  element.toString(),
            ),
    ));
    return null;
  }

  if ("navigationRequest" == call.method) {
    final url = call.arguments["url"] as String;
    final isForMainFrame = call.arguments["isForMainFrame"] as bool;

    if (NavigationDecision.navigate ==
        await settings.navigationDelegate!(NavigationRequest._(
          url: url,
          isForMainFrame: isForMainFrame,
        ))) return true;

    return false;
  }

  throw MissingPluginException("${call.method} was invoked but has no handler");
}
