import 'dart:async';

import 'package:flutter/material.dart';
import 'package:webview_without_view/webview_without_view.dart' as wv;

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  static const String url = "https://flutter.dev/";

  late String status;
  late bool isInitialized;
  late bool pageFinished;

  String? html;
  String? currentUrl;
  String? nullResult;
  String? nonNullResult;

  Future<void> readHTML() async {
    html = await wv.evaluateJavascript(
        "window.document.getElementsByTagName('html')[0].outerHTML;");

    // Somehow the '<' characters are retrieved as their
    // representive unicode value, so we have to replace
    // all occurences of the unicode value 003C with
    // the character '<'.
    //
    // If this is not done, then any CSS queries from
    // the [html](https://pub.dev/packages/html) will
    // not work, because of inproper HTML syntax.
    html = html?.replaceAll(r"\u003C", "<");

    // Replacing '\"' with '"' is not necessary for the CSS queries
    // to work, but it is probably useful if you plan to extract any
    // attribute values from any HTML tags.
    html = html?.replaceAll(r'\"', '"');

    print(html);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    status = "None";
    isInitialized = false;
    pageFinished = false;

    wv.settings
      ..onPageStarted.listen((event) {
        setState(() => status = "onPageStarted: $event");
        print(status);
      })
      ..onPageStarted.listen((event) {
        print("""


Another listener for onPageStarted


""");
      })
      ..onPageFinished.listen((event) {
        setState(() {
          pageFinished = true;
          status = "onPageFinished: $event";
        });
        print(status);
      })
      ..onProgress.listen((event) {
        print("onProgress: $event");
      })
      ..onWebResourceError.listen((event) {
        print("onWebResourceError: $event");
      });

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      wv.initialize().then((value) {
        setState(() {
          isInitialized = value;
          status = "wv.initialize: $value";
        });
        print(status);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    const margin = SizedBox(height: 35);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Webview Without View Example"),
      ),
      body: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Text(
                status,
                softWrap: true,
                style: Theme.of(context).textTheme.headline5,
              ),
              margin,
              ElevatedButton(
                onPressed: () {
                  wv.settings.javascriptMode = (wv.settings.javascriptMode ==
                          wv.JavascriptMode.unrestricted)
                      ? wv.JavascriptMode.disabled
                      : wv.JavascriptMode.unrestricted;
                  setState(() {});
                },
                child: Text(
                    "JS Mode: ${wv.JavascriptMode.values[wv.settings.javascriptMode.index]}"),
              ),
              if (isInitialized) ...[
                ElevatedButton(
                  onPressed: () => wv.loadUrl(url),
                  child: const Text("loadUrl"),
                ),
                ElevatedButton(
                  onPressed: () async {
                    currentUrl = await wv.getUrl();
                    setState(() {});
                  },
                  child: const Text("getUrl"),
                ),
                Text("URL: $currentUrl"),
                ElevatedButton(
                  onPressed: () async {
                    nullResult =
                        await wv.evaluateJavascript("I should return null.");
                    setState(() {});
                  },
                  child: const Text("nullResult"),
                ),
                Text("nullResult: $nullResult"),
                ElevatedButton(
                  onPressed: () async {
                    nonNullResult = await wv.evaluateJavascript("2 + 3");
                    setState(() {});
                  },
                  child: const Text("nonNullResult"),
                ),
                Text("nonNullResult: $nonNullResult"),
              ],
              if (isInitialized && pageFinished)
                ElevatedButton(
                  onPressed: readHTML,
                  child: const Text("readHTML"),
                ),
              margin,
              Text(html ?? ""),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    wv.dispose();
    super.dispose();
  }
}
