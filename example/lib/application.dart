import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'home.dart';

class Application extends StatelessWidget {
  static Future<void> run() async {
    //Tries to only allow Portrait mode, if an Error occures
    //it launches anyway but with Portrait and landscape
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    runApp(const Application._());
  }

  const Application._();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Home(),
    );
  }
}
