#import "WebviewWithoutViewPlugin.h"
#if __has_include(<webview_without_view/webview_without_view-Swift.h>)
#import <webview_without_view/webview_without_view-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "webview_without_view-Swift.h"
#endif

@implementation WebviewWithoutViewPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftWebviewWithoutViewPlugin registerWithRegistrar:registrar];
}
@end
